package SU_NSI;

import org.openqa.selenium.WebDriver;

public class SU_NSI_site {
    WebDriver webDriver;

    public SU_NSI_site (WebDriver driver)
    {
        webDriver = driver;
    }

    public SignInPage signInPage () {return new SignInPage(webDriver); }
    public FederalPage federalPage () {return new FederalPage(webDriver); }
}
