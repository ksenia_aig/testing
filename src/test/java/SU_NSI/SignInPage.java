package SU_NSI;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class SignInPage {
    WebDriver webDriver;
    WebDriverWait wait;

    @FindBy(id = "username")
    WebElement fieldLogin;

    @FindBy(id = "password")
    WebElement fieldPassword;

    @FindBy(id = "agree")
    WebElement checkBoxAgree;

    @FindBy(className= "smt-button")
    WebElement buttonSignIn;

    public SignInPage(WebDriver driver) {
        webDriver = driver;
        wait = new WebDriverWait(webDriver, 30);
        PageFactory.initElements(webDriver, this);
    }

    public void InputLogin(String login)
    {
        fieldLogin.sendKeys(login);
    }

    public void InputPassword(String password)
    {
        fieldLogin.sendKeys(password);
    }

    public void CheckAgree()
    {
        checkBoxAgree.click();
    }


    public  void ClickSingIn()
    {
        buttonSignIn.click();
    }

    public void WaitforResult() {
        webDriver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    }



}
