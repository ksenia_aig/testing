package SU_NSI;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class SignInTesting {

    WebDriver webDriver;
    SU_NSI_site su_nsi_site;

    @Before
    public void SetUp() {
        System.setProperty("webdriver.chrome.driver", "D://Java/chromedriver.exe");
        webDriver = new ChromeDriver();
        su_nsi_site = new SU_NSI_site(webDriver);
        webDriver.get("https://medved-nsi.hostco.ru/");
        System.out.println("1 - Open WebPage");
    }

    @Test
    public void ErrorSignIn() {
        su_nsi_site.signInPage().InputLogin("1111111111");
        su_nsi_site.signInPage().InputPassword("123");
        su_nsi_site.signInPage().CheckAgree();
        su_nsi_site.signInPage().ClickSingIn();
        Assert.assertNotNull("Message Not Found", webDriver.findElement(By.linkText()));
    }

    @Test
    public void CorrectSignIn() {
        su_nsi_site.signInPage().InputLogin("12352634745");
        su_nsi_site.signInPage().InputPassword("123");
        su_nsi_site.signInPage().CheckAgree();
        su_nsi_site.signInPage().ClickSingIn();
        su_nsi_site.signInPage().WaitforResult();
        String Url = webDriver.getCurrentUrl();
        Assert.assertEquals("https://medved-nsi.hostco.ru/federal", Url);
    }
    @After
    public void tearDown() {
        if (webDriver != null)
            webDriver.quit();
    }
}

